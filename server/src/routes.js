const home_handler = async (req, res) => {
	res.end("<h1>Node cors test </h1>")
}

const todos_handler = async (req, res) => {
	const todos = await fetch('https://jsonplaceholder.typicode.com/todos').then(resp => resp.json())
	res.json(todos)
}

const routes = [
	{
		path: '/',
		handler: home_handler
	},
	{
		path: '/todos',
		handler: todos_handler
	}
]

module.exports = routes