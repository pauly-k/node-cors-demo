const express = require('express')
const cors = require('cors')
const fetch = require('node-fetch')
const envvars = require('./envvars')
const cors_options = require('./cors_options')
const routes = require('./routes')

const app = express()

app.use(cors(cors_options))

routes.forEach(route => {
	app.get(route.path, route.handler)
})


const PORT = envvars.PORT
app.listen(PORT, () => {
	console.log('listening on port ', PORT)
})



