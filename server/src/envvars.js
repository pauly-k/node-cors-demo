require('dotenv').config()

if (!process.env.CORS_WHITELIST) {
	throw Error("process.env.CORS_WHITELIST not found")
}

module.exports = {
	whitelist: process.env.CORS_WHITELIST.split(/[\s,]+/),
	PORT: process.env.PORT || 8080
}
