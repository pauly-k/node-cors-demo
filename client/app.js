function query(selector) {
  return document.querySelector(selector)
}
// let url = 'https://jsonplaceholder.typicode.com/todos' 
let url = 'http://localhost:9888/todos'

getTodos(url)

async function getTodos(url) {
	const todosList = query('#todos-list')
  try {
    const resp = await fetch(url)
		const todos = await resp.json()
		console.log(todos);

    todosList.innerHTML = JSON.stringify(todos, null, 2)
  } catch (err) {
		console.log(err);
    todosList.innerHTML = err.message
  }
}
